import { Component } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  authState: any = null;

  constructor(public afAuth: AngularFireAuth) {
    // this.signUpWithEmail('metroid3.p2p@gmail.com', '123456');
    this.login('metroid3.p2p@gmail.com', '123456');
    // this.afAuth.auth.sendPasswordResetEmail('metroid.p2p@gmail.com');
    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth;
      console.log(this.authState);
      console.log(this.authState['email']);
    });
  }

  signUpWithEmail(email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    // return this.afAuth.auth.createUserAndRetrieveDataWithEmailAndPassword(email, password)
      .then((user) => {
        this.authState = user;
        console.log(user);
      })
      .catch(error => {
        console.log(error);
        throw error;
      });
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  login(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        this.authState = user;
      })
      .catch(error => {
        console.log(error);
        throw error;
      });
  }
}
